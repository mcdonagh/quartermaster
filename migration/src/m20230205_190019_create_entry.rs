use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(Entry::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Entry::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Entry::Item).string().not_null())
                    .col(ColumnDef::new(Entry::Price).money().not_null())
                    .col(ColumnDef::new(Entry::LocationId).integer())
                    .foreign_key(
                        ForeignKeyCreateStatement::new()
                            .name("fk-entry-location")
                            .from_tbl(Entry::Table)
                            .from_col(Entry::LocationId)
                            .to_tbl(Location::Table)
                            .to_col(Location::Id),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(Entry::Table).to_owned())
            .await
    }
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
enum Entry {
    Table,
    Id,
    Item,
    Price,
    LocationId,
}

#[derive(Iden)]
enum Location {
    Table,
    Id,
}
