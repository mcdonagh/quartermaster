pub use sea_orm_migration::prelude::*;

mod m20230129_205225_create_location;
mod m20230205_190019_create_entry;
mod m20230205_190020_create_tag;
mod m20230205_212040_create_entry_tag;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20230129_205225_create_location::Migration),
            Box::new(m20230205_190019_create_entry::Migration),
            Box::new(m20230205_190020_create_tag::Migration),
            Box::new(m20230205_212040_create_entry_tag::Migration),
        ]
    }
}
