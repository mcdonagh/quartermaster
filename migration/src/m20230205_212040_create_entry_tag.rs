use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(EntryTag::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(EntryTag::EntryId).integer().not_null())
                    .col(ColumnDef::new(EntryTag::TagId).integer().not_null())
                    .primary_key(
                        Index::create()
                            .name("pk-entry_tag")
                            .col(EntryTag::EntryId)
                            .col(EntryTag::TagId)
                            .primary(),
                    )
                    .foreign_key(
                        ForeignKeyCreateStatement::new()
                            .name("fk-entry-entry_tag")
                            .from_tbl(EntryTag::Table)
                            .from_col(EntryTag::EntryId)
                            .to_tbl(Entry::Table)
                            .to_col(Entry::Id),
                    )
                    .foreign_key(
                        ForeignKeyCreateStatement::new()
                            .name("fk-tag-entry_tag")
                            .from_tbl(EntryTag::Table)
                            .from_col(EntryTag::EntryId)
                            .to_tbl(Tag::Table)
                            .to_col(Tag::Id),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(EntryTag::Table).to_owned())
            .await
    }
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
enum EntryTag {
    Table,
    EntryId,
    TagId,
}

#[derive(Iden)]
enum Entry {
    Table,
    Id,
}

#[derive(Iden)]
enum Tag {
    Table,
    Id,
}
