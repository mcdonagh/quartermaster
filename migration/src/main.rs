use sea_orm_migration::prelude::*;

#[async_std::main]
async fn main() {
    utils::db_conn_string().expect("Could not get database connection string");
    cli::run_cli(migration::Migrator).await;
}
