use anyhow::Result;
use dirs;
use std::{
    env,
    ffi::OsString,
    fs,
    path::{Path, PathBuf},
};

/// gets the database connection string to use
/// and makes sure of the following:
/// * the environmental variable `DATABASE_URL` is set to the return value
/// * the directory of the file (not the file itself) exists
pub fn db_conn_string() -> Result<OsString> {
    match env::var("DATABASE_URL") {
        Ok(db_url) => Ok(OsString::from(db_url)),
        Err(env_err) => match data_path("db") {
            Ok(path_buf) => {
                let path = path_buf.into_os_string();

                let mut conn_string = OsString::from("sqlite:");
                conn_string.push(path);
                conn_string.push("?mode=rwc");

                env::set_var("DATABASE_URL", &conn_string);

                Ok(conn_string)
            }
            Err(data_path_err) => Err(data_path_err.context(env_err)),
        },
    }
}

/// gets the path to the data file whose name is `file_name` for the current OS or working directory
/// and makes sure the directory of the file (not the file itself) exists
pub fn data_path<P: AsRef<Path>>(file_name: P) -> Result<PathBuf> {
    let mut path = dirs::data_dir().unwrap_or_else(|| PathBuf::from("."));
    path.push("quatermaster");
    fs::create_dir_all(&path)?;
    path.push(file_name);
    Ok(path)
}
